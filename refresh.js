const axios = require('axios');
const fs=require('fs')

async function refresh() {
  const response = await axios.get('https://en.wikipedia.org/wiki/Table_of_keyboard_shortcuts')
  fs.writeFileSync('downloaded/table.html',response.data)
}
refresh()