#Shortcut Finder

This project uses https://en.wikipedia.org/wiki/Table_of_keyboard_shortcuts as a datasource of keyboard shortcuts and adds filtering and a keyboard visualization to help you find which keys are still available to be used in a keyboard interaction for your app.

##To Run

`mkdir downloaded`

`npm install`

`npm refresh`

`npm start`

Then open a browser and go to http://localhost:3000

##To get an updated version of the wikipedia page

`npm refresh`

#Screenshot

![Current Screenshot](currentShot.png)