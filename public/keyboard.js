Vue.component('keyboard',{
  props:{
    keys:{
      type:Array
    }
  },
  methods:{
    selectedClass(key) {
      return this.keys.includes(key) ? ['selected'] : ['available']
    }
  },
  //html and css for keyboard from: https://codepen.io/attilahajzer/pen/kydqJ
  template:/*html*/`
<div class="keyboard">
<legend>
<span class="selected">Taken Keys</span>
<span class="available">Available Keys</span>
</legend>
  <div class="section-a">
  <div class="key function space1">
    <span :class="selectedClass('Escape')">Esc</span>
  </div>

  <div class="key function">
  <span :class="selectedClass('F1')">F1</span>
  </div>
  <div class="key function">
  <span :class="selectedClass('F2')">F2</span>
  </div>
  <div class="key function">
  <span :class="selectedClass('F3')">F3</span>
  </div>
  
  <div class="key function space2">
  <span :class="selectedClass('F4')">F4</span>
  </div>
  <div class="key function">
  <span :class="selectedClass('F5')">F5</span>
  </div>
  <div class="key function">
  <span :class="selectedClass('F6')">F6</span>
  </div>
    <div class="key function">
    <span :class="selectedClass('F7')">F7</span>
  </div>
  <div class="key function space2">
  <span :class="selectedClass('F8')">F8</span>
  </div>
  
  <div class="key function">
  <span :class="selectedClass('F9')">F9</span>
  </div>
  <div class="key function">
  <span :class="selectedClass('F10')">F10</span>
  </div>
    <div class="key function">
    <span :class="selectedClass('F11')">F11</span>
  </div>
  <div class="key function">
  <span :class="selectedClass('F12')">F12</span>
  </div>
    <!--END FUNCTION KEYS -->
    
  <div class="key num dual">
    <span :class="selectedClass('~')">~</span>
    <br>
    <span :class="selectedClass('\`')">&#96;</span>
  </div>
    
  <div class="key num dual">
  <span :class="selectedClass('!')">!</span>
    <br>
    <span :class="selectedClass('1')">1</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('@')">@</span>  
  <br>
    <span :class="selectedClass('2')">2</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('#')">#</span>
    <br>
    <span :class="selectedClass('3')">3</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('$')">$</span>
    <br>
    <span :class="selectedClass('4')">4</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('%')">%</span>
    <br>
    <span :class="selectedClass('5')">5</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('^')">^</span>  
  <br>
  <span :class="selectedClass('6')">6</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('&')">&</span>
    <br>
    <span :class="selectedClass('7')">7</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('*')">*</span>
    <br>
    <span :class="selectedClass('8')">8</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('(')">(</span>
    <br>
    <span :class="selectedClass('9')">9</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass(')')">)</span>
    <br>
    <span :class="selectedClass('0')">0</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('_')">_</span>
    <br>
    <span :class="selectedClass('-')">-</span>
  </div>
  <div class="key num dual">
  <span :class="selectedClass('+')">+</span>
    <br>
    <span :class="selectedClass('=')">=</span>
  </div>
  <div class="key backspace">
  <span :class="selectedClass('Backspace')">Backspace</span>
  </div>
   <!--END NUMBER KEYS -->
    
  <div class="key tab">
  <span :class="selectedClass('Tab')">Tab</span>
  </div>
  
  <div class="key letter">
  <span :class="selectedClass('Q')">Q</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('W')">W</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('E')">E</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('R')">R</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('T')">T</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('Y')">Y</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('U')">U</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('I')">I</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('O')">O</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('P')">P</span>
  </div>
    <div class="key dual">
    <span :class="selectedClass('{')">{</span>
    <br>
    <span :class="selectedClass('[')">&#91;</span>
  </div>
    <div class="key dual">
    <span :class="selectedClass('}')">}</span>
    <br>
    <span :class="selectedClass(']')">&#93;</span>
  </div>
    <div class="key letter dual slash">
    <span :class="selectedClass('|')">|</span>
    <br>
    <span :class="selectedClass('\\\\')">&#92;</span>
  </div>
  <div class="key caps">
  <span :class="selectedClass('CapsLock')">Caps
    <br>Lock</span>
    </div>
  <div class="key letter">
  <span :class="selectedClass('A')">A</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('S')">S</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('D')">D</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('F')">F</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('G')">G</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('H')">H</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('J')">J</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('K')">K</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('L')">L</span>
  </div>
    <div class="key dual">
    <span :class="selectedClass(':')">:</span>
    <br>
    <span :class="selectedClass(';')">;</span>
  </div>
    <div class="key dual">
    <span :class='selectedClass(\`"\`)'>"</span>
    <br>
    <span :class="selectedClass(\`'\`)">'</span>
  </div>
    <div class="key enter">
    <span :class="selectedClass('Enter')">Enter</span>
  </div>
    
  <div class="key shift left">
  <span :class="selectedClass('Shift')">Shift</span>
  </div>
  <div class="key letter">
  <span :class="selectedClass('Z')">Z</span>
  </div>  
    <div class="key letter">
    <span :class="selectedClass('X')">X</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('C')">C</span>
  </div>
    <div class="key letter">
    <span :class="selectedClass('V')">V</span>
  </div><div class="key letter">
  <span :class="selectedClass('B')">B</span>
  </div><div class="key letter">
  <span :class="selectedClass('N')">N</span>
  </div><div class="key letter">
  <span :class="selectedClass('M')">M</span>
  </div>
    <div class="key dual">
    <span :class="selectedClass('<')"><</span>
    <br>
    <span :class="selectedClass(',')">,</span>
  </div>
    <div class="key dual">
    <span :class="selectedClass('>')">></span>
    <br>
    <span :class="selectedClass('.')">.</span>
  </div>
    <div class="key dual">
    <span :class="selectedClass('?')">?</span>
    <br>
    <span :class="selectedClass('/')">/</span>
  </div>
    <div class="key shift right">
    <span :class="selectedClass('Shift')">Shift</span>
  </div>
  <div class="key ctrl">
  <span :class="selectedClass('Control')">Ctrl</span>
  </div>
    <div class="key">
    <span :class="selectedClass('Meta')">⊞/⌘</span>
  </div>
    <div class="key">
    <span :class="selectedClass('Alt')">Alt</span>
  </div>
    <div class="key space">
    <span :class="selectedClass(' ')">Space</span>
  
 
    </div>
    <div class="key">
    <span :class="selectedClass('Alt')">Alt</span>
  </div>
    <div class="key">
    <span :class="selectedClass('Fn')">Fn</span>
  </div>
  <div class="key">
  <span :class="selectedClass('ContextMenu')">≣</span>
</div>
    <div class="key ctrl">
    <span :class="selectedClass('Control')">Ctrl</span>
  </div>
  </div><!-- end section-a-->
  
  <div class="section-b">
    <div class="key function small">
    <span :class="selectedClass('PrintScreen')">Prnt Scrn</span>
    </div>
    <div class="key function small">
    <span :class="selectedClass('ScrollLock')">Scroll Lock</span>
    </div>
    <div class="key function small">
    <span :class="selectedClass('Pause')">Pause Break</span>
    </div>
    
    <div class="sec-func">
    <div class="key">
    <span :class="selectedClass('Insert')">Insert</span>
    </div>
    <div class="key">
    <span :class="selectedClass('Home')">Home</span>
    </div>
    <div class="key dual">
    <span :class="selectedClass('PageUp')">Page Up</span>
    </div>
    <div class="key">
    <span :class="selectedClass('Delete')">Del</span>
    </div>
    <div class="key">
    <span :class="selectedClass('End')">End</span>
    </div>
      <div class="key dual">
      <span :class="selectedClass('PageDown')">Page Down</span>
    </div>
      
      <div class="arrows">
    <div class="key hidden">
      
    </div>
    <div class="key">
    <span :class="selectedClass('ArrowUp')">&#8593;</span>
    </div>
    <div class="key hidden">
      
    </div>
    <div class="key">
    <span :class="selectedClass('ArrowLeft')">&#8592;</span>
    </div>
    <div class="key">
    <span :class="selectedClass('ArrowDown')">&#8595;</span>
    </div>
      <div class="key">
      <span :class="selectedClass('ArrowRight')">&#8594;</span>
    </div>
      </div><!-- end arrows -->
    </div><!-- end sec-func -->
    
    
  </div><!-- end section-b-->
  
</div>
  `
})