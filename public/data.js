
const keys=["Enter","Tab"," ","ArrowDown","ArrowLeft","ArrowRight","ArrowUp","End","Home","PageDown","PageUp","Backspace","Clear","Copy","CrSel","Cut","Delete","EraseOf","ExSel","Insert","Paste","Redo","Undo","Escape","Find","Help","Pause","Play","ContextMenu","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14","F15","F16","F17","F18","F19","F20","PrintScreen","MediaFastForward","MediaPause","MediaPlay","MediaPlayPause","MediaRecord","MediaRewind","MediaStop","MediaTrackNext","MediaTrackPrevious","Multiply","Add","Clear","Divide","Subtract","Alt","CapsLock","Control","Fn","Hyper","FnLock","Meta","NumLock","ScrollLock","Shift","Super","Symbol","SymbolLock","`","~","1","!","2","@","3","#","4","$","5","%","6","^","7","&","8","*","9","(","0",")","-","_","=","+","[","{","]","}",";",":","'","\"","\\","|","<",">",",",".","/","?","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"]
const data={
  "General shortcuts": {
    "Chrome OS": [
      {
        "Control+O": "File menu\n"
      },
      {
        "Control+Z": "Undo the last operation\n"
      },
      {
        "Control+Shift+Z": "Redo the last operation\n"
      },
      {
        "Control+X": "Cut the selection and store it in the clipboard\n"
      },
      {
        "Control+C": "Copy the selection into the clipboard\n"
      },
      {
        "Control+V": "Paste contents of clipboard at cursor\n"
      },
      {
        "Control+Shift+V": "Paste special\n"
      },
      {
        "Control+A": "Select all in focused control or window\n"
      },
      {
        "Control+Space": "Cycle through installed keyboard languages / input methods\n"
      },
      {
        "Control+Alt+/": "Open Help Menu\n"
      }
    ],
    "Vim": [
      {
        "Alt+F": "File menu\n"
      },
      {
        "Alt+E": "Edit menu\n"
      },
      {
        "U": "Undo the last operation\n"
      },
      {
        "Control+R": "Redo the last operation\n"
      },
      {
        "X": "Cut the selection and store it in the clipboard\n"
      },
      {
        "Y": "Copy the selection into the clipboard\n"
      },
      {
        "P": "Paste contents of clipboard at cursor\n"
      },
      {
        "Control+Shift+V": "Paste special\n"
      },
      {
        "ggVG+%+:%y": "Select all in focused control or window\n"
      }
    ],
    "Emacs": [
      {
        "Meta+`": "File menu\n"
      },
      {
        "Meta+`": "Edit menu\n"
      },
      {
        "Control+X": "Undo the last operation\n"
      },
      {
        "Control+W": "Cut the selection and store it in the clipboard\n"
      },
      {
        "Meta+W": "Copy the selection into the clipboard\n"
      },
      {
        "Control+Ins": "Copy the selection into the clipboard\n"
      },
      {
        "Control+Y": "Paste contents of clipboard at cursor\n"
      },
      {
        "Shift+Ins": "Paste contents of clipboard at cursor\n"
      },
      {
        "Meta+Y": "Paste special\n"
      },
      {
        "Control+X": "Select all in focused control or window\n"
      },
      {
        "Control+\\": "Cycle through installed keyboard languages / input methods\n"
      }
    ],
    "KDE/GNOME": [
      {
        "Alt+F": "File menu\n"
      },
      {
        "Alt+E": "Edit menu\n"
      },
      {
        "Alt+V": "View menu\n"
      },
      {
        "Control+Z": "Undo the last operation\n"
      },
      {
        "Control+Shift+Z": "Redo the last operation\n"
      },
      {
        "Control+Y": "Redo the last operation\n"
      },
      {
        "Control+X": "Cut the selection and store it in the clipboard\n"
      },
      {
        "Control+C": "Copy the selection into the clipboard\n"
      },
      {
        "Control+V": "Paste contents of clipboard at cursor\n"
      },
      {
        "Control+Shift+V": "Paste special\n"
      },
      {
        "Control+A": "Select all in focused control or window\n"
      },
      {
        "Control+Alt+K": "Cycle through installed keyboard languages / input methods\n"
      },
      {
        "Control+P": "Print "
      }
    ],
    "macOS": [
      {
        "Control+F2": "File menu\n"
      },
      {
        "Control+F2": "Edit menu\n"
      },
      {
        "Control+F2": "View menu\n"
      },
      {
        "Meta+Z": "Undo the last operation\n"
      },
      {
        "Shift+Meta+Z": "Redo the last operation\n"
      },
      {
        "Meta+X": "Cut the selection and store it in the clipboard\n"
      },
      {
        "Meta+C": "Copy the selection into the clipboard\n"
      },
      {
        "Meta+V": "Paste contents of clipboard at cursor\n"
      },
      {
        "Shift+Opt+Meta+V": "Paste special\n"
      },
      {
        "Meta+A": "Select all in focused control or window\n"
      },
      {
        "Meta+Space": "Cycle through installed keyboard languages / input methods\n"
      },
      {
        "Meta+P": "Print "
      }
    ],
    "Windows": [
      {
        "Alt+F": "File menu\n"
      },
      {
        "F10": "File menu\n"
      },
      {
        "Alt+E": "Edit menu\n"
      },
      {
        "Alt+V": "View menu\n"
      },
      {
        "Control+Z": "Undo the last operation\n"
      },
      {
        "Alt+Backspace": "Undo the last operation\n"
      },
      {
        "Control+Y": "Redo the last operation\n"
      },
      {
        "Alt+Shift+Backspace": "Redo the last operation\n"
      },
      {
        "Shift+Delete": "Cut the selection and store it in the clipboard\n"
      },
      {
        "Control+Ins": "Copy the selection into the clipboard\n"
      },
      {
        "Shift+Ins": "Paste contents of clipboard at cursor\n"
      },
      {
        "Meta+V": "Paste special\n"
      },
      {
        "Control+A": "Select all in focused control or window\n"
      },
      {
        "Alt+Shift": "Cycle through installed keyboard languages / input methods\n"
      },
      {
        "Meta+Space": "Cycle through installed keyboard languages / input methods\n"
      },
      {
        "Control+P": "Print "
      }
    ]
  },
  "Navigation": {
    "Chrome OS": [
      {
        "Search": "Applications menu\n"
      },
      {
        "Power": "Lock desktop\n"
      },
      {
        "Control+Shift+Q": "Log out user\n"
      },
      {
        "Search+Escape": "Task manager\n"
      },
      {
        "Alt": "Switch window (next/previous)\n"
      },
      {
        "Search": "Search "
      }
    ],
    "KDE/GNOME": [
      {
        "Control+N": "New browser window with same page as current\n"
      },
      {
        "Control+Shift+N": "New folder\n"
      },
      {
        "Alt+A": "Applications menu\n"
      },
      {
        "Alt+B": "Applications menu\n"
      },
      {
        "Alt+C": "Applications menu\n"
      },
      {
        "Alt+D": "Applications menu\n"
      },
      {
        "Alt+E": "Applications menu\n"
      },
      {
        "Alt+F": "Applications menu\n"
      },
      {
        "Alt+G": "Applications menu\n"
      },
      {
        "Alt+H": "Applications menu\n"
      },
      {
        "Alt+I": "Applications menu\n"
      },
      {
        "Alt+J": "Applications menu\n"
      },
      {
        "Alt+K": "Applications menu\n"
      },
      {
        "Alt+L": "Applications menu\n"
      },
      {
        "Alt+M": "Applications menu\n"
      },
      {
        "Alt+N": "Applications menu\n"
      },
      {
        "Alt+O": "Applications menu\n"
      },
      {
        "Alt+P": "Applications menu\n"
      },
      {
        "Alt+Q": "Applications menu\n"
      },
      {
        "Alt+R": "Applications menu\n"
      },
      {
        "Alt+S": "Applications menu\n"
      },
      {
        "Alt+T": "Applications menu\n"
      },
      {
        "Alt+U": "Applications menu\n"
      },
      {
        "Alt+V": "Applications menu\n"
      },
      {
        "Alt+W": "Applications menu\n"
      },
      {
        "Alt+X": "Applications menu\n"
      },
      {
        "Alt+Y": "Applications menu\n"
      },
      {
        "Alt+Z": "Applications menu\n"
      },
      {
        "Control+Alt+L": "Lock desktop\n"
      },
      {
        "Control+Alt+D": "Show desktop\n"
      },
      {
        "Control+Super+D": "Minimize all windows\n"
      },
      {
        "Control+Escape": "Task manager\n"
      },
      {
        "F2": "Rename object\n"
      },
      {
        "Enter": "Open file or program\n"
      },
      {
        "Control+Tab": "Switch window (next/previous)\n"
      },
      {
        "Control+Escape": "Switch window without dialog (next/previous)\n"
      },
      {
        "Alt+F2": "Run application\n"
      }
    ],
    "macOS": [
      {
        "Meta+T": "New browser window with same page as current\n"
      },
      {
        "Shift+Meta+N": "New folder\n"
      },
      {
        "Control+F2": "Applications menu\n"
      },
      {
        "Control+Meta+Q+Control+Shift+Eject+Control+Shift+Pwr": "Lock desktop\n"
      },
      {
        "F11": "Show desktop\n"
      },
      {
        "Shift+Meta+Q": "Log out user\n"
      },
      {
        "⌥ Opt+Meta+Escape": "Task manager\n"
      },
      {
        "Enter+F2": "Rename object\n"
      },
      {
        "Meta+O": "Open file or program\n"
      },
      {
        "Meta+ArrowDown": "Open file or program\n"
      },
      {
        "Meta+Tab": "Switch window (next/previous)\n"
      },
      {
        "Meta+Space+Meta+ArrowDown": "Run application\n"
      },
      {
        "Meta+Space": "Search "
      },
      {
        "Shift+Meta+.": "Show Hidden Files\n"
      }
    ],
    "Windows": [
      {
        "Control+N": "New browser window with same page as current\n"
      },
      {
        "Control+Shift+N": "New folder\n"
      },
      {
        "Meta": "Applications menu\n"
      },
      {
        "Control+Escape": "Applications menu\n"
      },
      {
        "Meta+L": "Lock desktop\n"
      },
      {
        "Meta+D": "Show desktop\n"
      },
      {
        "Meta+M": "Minimize all windows\n"
      },
      {
        "Meta+L": "Switch active user\n"
      },
      {
        "Control+Shift+Escape": "Task manager\n"
      },
      {
        "F2": "Rename object\n"
      },
      {
        "Enter": "Open file or program\n"
      },
      {
        "Shift+Enter": "Open file or program\n"
      },
      {
        "Alt+Tab": "Switch window (next/previous)\n"
      },
      {
        "Alt+Escape": "Switch window without dialog (next/previous)\n"
      },
      {
        "Meta": "Run application\n"
      },
      {
        "Meta+Meta+S": "Search "
      },
      {
        "F3": "Search "
      }
    ]
  },
  "Power management": {
    "Chrome OS": [
      {
        "Search+Escape": "Task manager/Force quit\n"
      },
      {
        "Power": "Shut down computer\n"
      },
      {
        "refresh+power": "Force shutdown\n"
      }
    ],
    "KDE/GNOME": [
      {
        "Sleep": "Place computer into sleep/standby mode\n"
      },
      {
        "Control+Alt+Shift+PageDown": "Shut down computer\n"
      },
      {
        "Control+Alt+Shift+PageUp": "Restart computer\n"
      }
    ],
    "macOS": [
      {
        "⌥ Opt+Meta+Eject": "Place computer into sleep/standby mode\n"
      },
      {
        "Meta+⌥ Opt+Escape": "Task manager/Force quit\n"
      },
      {
        "Control+⌥ Opt+Meta+Eject": "Shut down computer\n"
      },
      {
        "Control+Meta+Eject+Control+Meta+Power": "Restart computer\n"
      },
      {
        "Control+Shift+Eject": "Place display in sleep mode\n"
      },
      {
        "Control+Eject": "Bring up power/sleep dialog box\n"
      },
      {
        "⌥ Opt+Meta+Eject": "Force shutdown\n"
      }
    ],
    "Windows": [
      {
        "Meta+X+U+S": "Place computer into sleep/standby mode\n"
      },
      {
        "Control+Shift+Escape": "Task manager/Force quit\n"
      },
      {
        "Meta+X+U+U": "Shut down computer\n"
      },
      {
        "Meta+X+U+R+Meta+ArrowRight+ArrowRight+ArrowUp+Enter": "Restart computer\n"
      },
      {
        "Power": "Force shutdown\n"
      }
    ]
  },
  "Screenshots": {
    "KDE/GNOME": [
      {
        "PrintScreen": "Save screenshot of entire screen as file\n"
      },
      {
        "Control+PrintScreen": "Copy screenshot of entire screen to clipboard\n"
      },
      {
        "Control+Alt+PrintScreen": "Copy screenshot of active window to clipboard\n"
      },
      {
        "Alt+PrintScreen": "Save screenshot of window as file\n"
      },
      {
        "Shift+PrintScreen": "Save screenshot of arbitrary area as file\n"
      },
      {
        "Alt+PrintScreen": "Copy screenshot of window to clipboard\n"
      },
      {
        "Shift+PrintScreen": "Copy screenshot of arbitrary area to clipboard\n"
      },
      {
        "Control+Alt+Shift+R": "Screencasting "
      }
    ],
    "macOS": [
      {
        "Shift+Meta+3": "Save screenshot of entire screen as file\n"
      },
      {
        "Control+Shift+Meta+3": "Copy screenshot of entire screen to clipboard\n"
      },
      {
        "Shift+Meta+4": "Save screenshot of window as file\n"
      },
      {
        "Shift+Meta+4": "Save screenshot of arbitrary area as file\n"
      },
      {
        "Control+Shift+Meta+4": "Copy screenshot of window to clipboard\n"
      },
      {
        "Control+Shift+Meta+4": "Copy screenshot of arbitrary area to clipboard\n"
      },
      {
        "Shift+Meta+5": "Screenshot Utility\n"
      }
    ],
    "Windows": [
      {
        "Meta+PrintScreen": "Save screenshot of entire screen as file\n"
      },
      {
        "Meta+PrintScreen": "Copy screenshot of entire screen to clipboard\n"
      },
      {
        "PrintScreen": "Copy screenshot of entire screen to clipboard\n"
      },
      {
        "Alt+PrintScreen": "Copy screenshot of active window to clipboard\n"
      },
      {
        "Meta+Shift+S": "Copy screenshot of arbitrary area to clipboard\n"
      }
    ]
  },
  "Text editing": {
    "Chrome OS": [
      {
        "Alt+Backspace": "Delete char to the right of cursor\n"
      },
      {
        "Delete": "Delete char to the right of cursor\n"
      },
      {
        "Backspace": "Delete word to the left of cursor\n"
      },
      {
        "Control+search+ArrowLeft": "Go to start of document\n"
      },
      {
        "Control+search+ArrowRight": "Go to end of document\n"
      },
      {
        "Control+F": "Find "
      }
    ],
    "Vim": [
      {
        "X": "Delete char to the right of cursor\n"
      },
      {
        "dw": "Delete word to the right of cursor\n"
      },
      {
        "dge": "Delete word to the left of cursor\n"
      },
      {
        "^+0": "Go to start of line\n"
      },
      {
        "$": "Go to end of line\n"
      },
      {
        "gg": "Go to start of document\n"
      },
      {
        "G": "Go to end of document\n"
      },
      {
        "B": "Go to previous word\n"
      },
      {
        "ge": "Go to previous word\n"
      },
      {
        "W": "Go to next word\n"
      },
      {
        "E": "Go to next word\n"
      },
      {
        "K": "Go to previous line\n"
      },
      {
        "ArrowUp": "Go to previous line\n"
      },
      {
        "J": "Go to next line\n"
      },
      {
        "ArrowDown": "Go to next line\n"
      },
      {
        "(": "Go to previous line break (paragraph)\n"
      },
      {
        ")": "Go to next line break\n"
      },
      {
        "Control+F": "Move the cursor down the length of the viewport\n"
      },
      {
        "Control+B": "Move the cursor up the length of the viewport\n"
      },
      {
        "/": "Find "
      },
      {
        "N": "Go to next search result\n"
      },
      {
        "N": "Go to previous search result\n"
      },
      {
        "%s/fosh/fish/gc": "Search and replace\n"
      }
    ],
    "Emacs": [
      {
        "Control+D": "Delete char to the right of cursor\n"
      },
      {
        "Meta+D": "Delete word to the right of cursor\n"
      },
      {
        "Control+A": "Go to start of line\n"
      },
      {
        "Control+E": "Go to end of line\n"
      },
      {
        "Meta+<": "Go to start of document\n"
      },
      {
        "Meta+>": "Go to end of document\n"
      },
      {
        "Meta+B": "Go to previous word\n"
      },
      {
        "Meta+F": "Go to next word\n"
      },
      {
        "Control+P": "Go to previous line\n"
      },
      {
        "ArrowUp": "Go to previous line\n"
      },
      {
        "Control+N": "Go to next line\n"
      },
      {
        "ArrowDown": "Go to next line\n"
      },
      {
        "Meta+(": "Go to previous line break (paragraph)\n"
      },
      {
        "Control+ArrowUp": "Go to previous line break (paragraph)\n"
      },
      {
        "Meta+}": "Go to next line break\n"
      },
      {
        "Control+ArrowDown": "Go to next line break\n"
      },
      {
        "Control+V": "Move the cursor down the length of the viewport\n"
      },
      {
        "Meta+V": "Move the cursor up the length of the viewport\n"
      },
      {
        "Control+S": "Find "
      },
      {
        "Control+S": "Go to next search result\n"
      },
      {
        "Control+R": "Go to previous search result\n"
      },
      {
        "Meta+%": "Search and replace\n"
      },
      {
        "Control+Meta+S": "Search with a regular expression\n"
      },
      {
        "Control+Meta+%": "Search and replace with a regular expression\n"
      }
    ],
    "KDE/GNOME": [
      {
        "Delete": "Delete char to the right of cursor\n"
      },
      {
        "Control+Delete": "Delete word to the right of cursor\n"
      },
      {
        "Home": "Go to start of line\n"
      },
      {
        "End": "Go to end of line\n"
      },
      {
        "Control+Home": "Go to start of document\n"
      },
      {
        "Control+End": "Go to end of document\n"
      },
      {
        "Control+ArrowLeft": "Go to previous word\n"
      },
      {
        "Control+ArrowRight": "Go to next word\n"
      },
      {
        "ArrowUp": "Go to previous line\n"
      },
      {
        "ArrowDown": "Go to next line\n"
      },
      {
        "Control+ArrowUp": "Go to previous line break (paragraph)\n"
      },
      {
        "Control+ArrowDown": "Go to next line break\n"
      },
      {
        "PageDown": "Move the cursor down the length of the viewport\n"
      },
      {
        "PageUp": "Move the cursor up the length of the viewport\n"
      },
      {
        "Control+F": "Find "
      },
      {
        "Control+G": "Go to next search result\n"
      },
      {
        "Control+H": "Search and replace\n"
      }
    ],
    "macOS": [
      {
        "Delete": "Delete char to the right of cursor\n"
      },
      {
        "⌥ Opt+Delete": "Delete word to the right of cursor\n"
      },
      {
        "Meta+ArrowLeft": "Go to start of line\n"
      },
      {
        "Meta+ArrowRight": "Go to end of line\n"
      },
      {
        "Meta+ArrowUp": "Go to start of document\n"
      },
      {
        "Meta+ArrowDown": "Go to end of document\n"
      },
      {
        "⌥ Opt+ArrowLeft": "Go to previous word\n"
      },
      {
        "⌥ Opt+ArrowRight": "Go to next word\n"
      },
      {
        "ArrowUp": "Go to previous line\n"
      },
      {
        "Control+P": "Go to previous line\n"
      },
      {
        "ArrowDown": "Go to next line\n"
      },
      {
        "Control+N": "Go to next line\n"
      },
      {
        "⌥ Opt+ArrowUp": "Go to previous line break (paragraph)\n"
      },
      {
        "⌥ Opt+ArrowDown": "Go to next line break\n"
      },
      {
        "⌥ Opt+⇟": "Move the cursor down the length of the viewport\n"
      },
      {
        "⌥ Opt+⇞": "Move the cursor up the length of the viewport\n"
      },
      {
        "Meta+F": "Find "
      },
      {
        "Meta+G": "Go to next search result\n"
      },
      {
        "Meta+F": "Search and replace\n"
      }
    ],
    "Windows": [
      {
        "Delete": "Delete char to the right of cursor\n"
      },
      {
        "Control+Delete": "Delete word to the right of cursor\n"
      },
      {
        "Home": "Go to start of line\n"
      },
      {
        "End": "Go to end of line\n"
      },
      {
        "Control+Home": "Go to start of document\n"
      },
      {
        "Control+End": "Go to end of document\n"
      },
      {
        "Control+ArrowLeft": "Go to previous word\n"
      },
      {
        "Control+ArrowRight": "Go to next word\n"
      },
      {
        "ArrowUp": "Go to previous line\n"
      },
      {
        "ArrowDown": "Go to next line\n"
      },
      {
        "Control+ArrowUp": "Go to previous line break (paragraph)\n"
      },
      {
        "Control+ArrowDown": "Go to next line break\n"
      },
      {
        "PageDown": "Move the cursor down the length of the viewport\n"
      },
      {
        "PageUp": "Move the cursor up the length of the viewport\n"
      },
      {
        "Control+F": "Find "
      },
      {
        "F3": "Go to next search result\n"
      },
      {
        "Shift+F3": "Go to previous search result\n"
      },
      {
        "Control+H": "Search and replace\n"
      }
    ]
  },
  "Text formatting": {
    "Vim": [
      {
        "gU+gu+~": "Uppercase / Lowercase\n"
      }
    ],
    "Emacs": [
      {
        "Meta+O": "Bold "
      },
      {
        "Meta+O": "Underline "
      },
      {
        "Meta+O": "Italic "
      },
      {
        "Meta+U": "Uppercase / Lowercase\n"
      }
    ],
    "KDE/GNOME": [
      {
        "Control+B": "Bold "
      },
      {
        "Control+U": "Underline "
      },
      {
        "Control+I": "Italic "
      },
      {
        "Shift+F3": "Uppercase / Lowercase\n"
      },
      {
        "Control+Shift+P": "Superscript "
      },
      {
        "Control+Shift+B": "Subscript "
      },
      {
        "Control+Enter": "Insert Linebreak\n"
      }
    ],
    "macOS": [
      {
        "Meta+B": "Bold "
      },
      {
        "Meta+U": "Underline "
      },
      {
        "Meta+I": "Italic "
      },
      {
        "⌥ Opt+Meta+C": "Uppercase / Lowercase\n"
      },
      {
        "Control+Meta+'+'": "Superscript "
      },
      {
        "Control+Meta+-": "Subscript "
      },
      {
        "⌥ Opt+Shift+>+⌥ Opt+Shift+<": "Selected text larger/smaller\n"
      },
      {
        "⌥ Opt+Enter": "Insert Linebreak\n"
      }
    ],
    "Windows WordPad": [
      {
        "Control+B": "Bold "
      },
      {
        "Control+U": "Underline "
      },
      {
        "Control+I": "Italic "
      },
      {
        "Control+Shift+A": "Uppercase / Lowercase\n"
      },
      {
        "Control+Shift+=": "Superscript "
      },
      {
        "Control+=": "Subscript "
      },
      {
        "Control+Shift+>+Control+Shift+<": "Selected text larger/smaller\n"
      },
      {
        "Control+Shift+L": "Selected text Bullets or Numbered Items\n"
      },
      {
        "Control+Enter": "Insert Linebreak\n"
      }
    ]
  },
  "Browsers / Go menu": {
    "Chrome OS": [
      {
        "Control+L": "Go to Address Bar\n"
      },
      {
        "Alt+D": "Go to Address Bar\n"
      },
      {
        "ArrowLeft": "Go to the previous location in history[13]\n"
      },
      {
        "ArrowRight": "Go to the next location in history\n"
      }
    ],
    "KDE/GNOME": [
      {
        "Control+L": "Go to Address Bar\n"
      },
      {
        "Alt+D": "Go to Address Bar\n"
      },
      {
        "F6": "Go to Address Bar\n"
      },
      {
        "Alt+ArrowLeft": "Go to the previous location in history[13]\n"
      },
      {
        "Alt+ArrowRight": "Go to the next location in history\n"
      },
      {
        "Alt+ArrowUp": "Go up one level in the navigation hierarchy\n"
      },
      {
        "Control+Home": "Go to the starting page defined by the user or application\n"
      }
    ],
    "macOS": [
      {
        "Meta+L": "Go to Address Bar\n"
      },
      {
        "Shift+Meta+G": "Go to Address Bar\n"
      },
      {
        "Meta+[": "Go to the previous location in history[13]\n"
      },
      {
        "Meta+ArrowLeft": "Go to the previous location in history[13]\n"
      },
      {
        "Meta+]": "Go to the next location in history\n"
      },
      {
        "Meta+ArrowRight": "Go to the next location in history\n"
      },
      {
        "Meta+ArrowUp": "Go up one level in the navigation hierarchy\n"
      },
      {
        "Meta+Home": "Go to the starting page defined by the user or application\n"
      }
    ],
    "Windows": [
      {
        "Alt+D": "Go to Address Bar\n"
      },
      {
        "Alt+C": "Go to Address Bar\n"
      },
      {
        "F6": "Go to Address Bar\n"
      },
      {
        "Alt+ArrowLeft": "Go to the previous location in history[13]\n"
      },
      {
        "Backspace": "Go to the previous location in history[13]\n"
      },
      {
        "Alt+ArrowRight": "Go to the next location in history\n"
      },
      {
        "Shift+Backspace": "Go to the next location in history\n"
      },
      {
        "Alt+ArrowUp+Backspace": "Go up one level in the navigation hierarchy\n"
      },
      {
        "Alt+Home": "Go to the starting page defined by the user or application\n"
      }
    ]
  },
  "Web browsers": {
    "Chrome OS": [
      {
        "Control+Enter": "URL Shortcuts (Adds www. + .com)\n"
      },
      {
        "Control+D": "Add bookmark for current page\n"
      },
      {
        "Control+E": "Focus and select Web search bar\n"
      },
      {
        "Control+K": "Focus and select Web search bar\n"
      },
      {
        "Control+L": "Focus and select address bar\n"
      },
      {
        "Alt+D": "Focus and select address bar\n"
      },
      {
        "⟲": "Refresh a webpage\n"
      },
      {
        "Control+R": "Refresh a webpage\n"
      },
      {
        "Control+Shift+R": "Refresh a webpage ignoring cache\n"
      },
      {
        "Control+N": "Open a new window\n"
      }
    ],
    "Vimperator": [
      {
        ":bmarks": "Bookmarks menu\n"
      },
      {
        ":bmark": "Add bookmark for current page\n"
      },
      {
        "T+T": "Focus and select Web search bar\n"
      },
      {
        "O+Y": "Focus and select address bar\n"
      },
      {
        "R": "Refresh a webpage\n"
      },
      {
        "R": "Refresh a webpage ignoring cache\n"
      },
      {
        ":winopen": "Open a new window\n"
      },
      {
        "zi": "Zoom Options (zoom in / zoom out / zoom 100%)\n"
      },
      {
        "zo": "Zoom Options (zoom in / zoom out / zoom 100%)\n"
      },
      {
        "zz": "Zoom Options (zoom in / zoom out / zoom 100%)\n"
      }
    ],
    "Emacs-w3m": [
      {
        "V": "Bookmarks menu\n"
      },
      {
        "A": "Add bookmark for current page\n"
      },
      {
        "Meta+A": "Add bookmark for current link\n"
      },
      {
        "G": "Focus and select address bar\n"
      },
      {
        "R": "Refresh a webpage\n"
      }
    ],
    "KDE/GNOME": [
      {
        "Alt+B": "Bookmarks menu\n"
      },
      {
        "Control+Enter": "URL Shortcuts (Adds www. + .com)\n"
      },
      {
        "Shift+Enter": "URL Shortcuts (Adds www. + .net)\n"
      },
      {
        "Control+B": "Add bookmark for current page\n"
      },
      {
        "Control+K": "Focus and select Web search bar\n"
      },
      {
        "Control+L": "Focus and select address bar\n"
      },
      {
        "F5": "Refresh a webpage\n"
      },
      {
        "Control+N": "Open a new window\n"
      },
      {
        "Control+'+'": "Zoom Options (zoom in / zoom out / zoom 100%)\n"
      }
    ],
    "macOS": [
      {
        "Meta+B": "Bookmarks menu\n"
      },
      {
        "Meta+Enter": "URL Shortcuts (Adds www. + .com)\n"
      },
      {
        "Shift+Enter": "URL Shortcuts (Adds www. + .net)\n"
      },
      {
        "Meta+D": "Add bookmark for current page\n"
      },
      {
        "Meta+E": "Focus and select Web search bar\n"
      },
      {
        "Meta+L": "Focus and select address bar\n"
      },
      {
        "Meta+R": "Refresh a webpage\n"
      },
      {
        "Meta+N": "Open a new window\n"
      },
      {
        "Meta+'+'": "Zoom Options (zoom in / zoom out / zoom 100%)\n"
      }
    ],
    "Windows": [
      {
        "Control+B": "Bookmarks menu\n"
      },
      {
        "Enter": "URL Shortcuts (Adds www. + .com)\n"
      },
      {
        "Shift+Enter": "URL Shortcuts (Adds www. + .net)\n"
      },
      {
        "Control+D": "Add bookmark for current page\n"
      },
      {
        "Control+B": "Manage bookmarks\n"
      },
      {
        "Control+E": "Focus and select Web search bar\n"
      },
      {
        "Control+L": "Focus and select address bar\n"
      },
      {
        "Fn+F5": "Refresh a webpage\n"
      },
      {
        "Control+F5": "Refresh a webpage ignoring cache\n"
      },
      {
        "Control+N": "Open a new window\n"
      },
      {
        "Control+'+'": "Zoom Options (zoom in / zoom out / zoom 100%)\n"
      }
    ]
  },
  "Tab management": {
    "Chrome OS": [
      {
        "Control+T": "New tab\n"
      },
      {
        "Control+W": "Close tab\n"
      },
      {
        "Control+Tab": "Go to next tab\n"
      },
      {
        "Control+Shift+Tab": "Go to previous tab\n"
      },
      {
        "Control+B": "Go to tab-n\n"
      },
      {
        "Control+Shift+T": "Open a previously closed tab\n"
      },
      {
        "Control+Shift+T": "Open a previously closed window\n"
      }
    ],
    "Vimperator": [
      {
        "T": "New tab\n"
      },
      {
        "D": "Close tab\n"
      },
      {
        "gt": "Go to next tab\n"
      },
      {
        "gT": "Go to previous tab\n"
      },
      {
        "g0": "Go to tab-n\n"
      },
      {
        "U": "Open a previously closed tab\n"
      }
    ],
    "Emacs-w3m": [
      {
        "Control+C": "New tab\n"
      },
      {
        "Control+C": "Close all tabs but the current one\n"
      },
      {
        "Control+C": "Go to next tab\n"
      },
      {
        "Control+C": "Go to previous tab\n"
      }
    ],
    "KDE/GNOME": [
      {
        "Control+W": "Close tab\n"
      },
      {
        "Control+PageDown": "Go to next tab\n"
      },
      {
        "Control+PageUp": "Go to previous tab\n"
      },
      {
        "Alt+N": "Go to tab-n\n"
      }
    ],
    "macOS": [
      {
        "Meta+T": "New tab\n"
      },
      {
        "Meta+W": "Close tab\n"
      },
      {
        "Control+Tab": "Go to next tab\n"
      },
      {
        "Meta+N": "Go to tab-n\n"
      }
    ],
    "Windows": [
      {
        "Control+T": "New tab\n"
      },
      {
        "Control+W": "Close tab\n"
      },
      {
        "Control+Tab": "Go to next tab\n"
      },
      {
        "Control+N": "Go to tab-n\n"
      }
    ]
  },
  "Window management": {
    "Chrome OS": [
      {
        "Control+W": "Close the current internet tab\n"
      },
      {
        "Control+W": "Close the focused window\n"
      },
      {
        "Control+Shift+W": "Close all windows of current application\n"
      },
      {
        "Alt+-": "Minimize the focused window\n"
      },
      {
        "Alt+'+'": "Maximize the focused window\n"
      },
      {
        "Fullscreen": "Switch fullscreen/normal size\n"
      },
      {
        "Control+Shift+W": "Quit application of current window\n"
      },
      {
        "Control+Shift+W": "Close dialog\n"
      },
      {
        "Alt+0": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+1": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+2": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+3": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+4": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+5": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+6": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+7": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+8": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+9": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Alt+0": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+1": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+2": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+3": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+4": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+5": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+6": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+7": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+8": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+9": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Alt+0": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+1": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+2": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+3": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+4": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+5": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+6": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+7": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+8": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Alt+9": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Control+Fullscreen": "External display options (mirror, extend desktop, etc.)\n"
      }
    ],
    "Emacs": [
      {
        "Meta+`": "Pop up window menu\n"
      },
      {
        "Control+X": "Close the focused window\n"
      },
      {
        "Control+X": "Restore the focused window to its previous size\n"
      },
      {
        "Control+X": "Resize the focused window\n"
      },
      {
        "Meta+X": "Hide the focused window\n"
      },
      {
        "Meta+X": "Minimize the focused window\n"
      },
      {
        "Control+X": "Maximize the focused window\n"
      },
      {
        "Control+X": "Show all open windows\n"
      },
      {
        "Control+X": "Show all windows of current application\n"
      },
      {
        "Q": "Quit application of current window\n"
      }
    ],
    "GNOME": [
      {
        "Alt+Space": "Pop up window menu\n"
      },
      {
        "Alt+F4": "Close the focused window\n"
      },
      {
        "Alt+F5": "Restore the focused window to its previous size\n"
      },
      {
        "Alt": "Move the focused window\n"
      },
      {
        "Alt+F8": "Resize the focused window\n"
      },
      {
        "Alt+F9": "Minimize the focused window\n"
      },
      {
        "Alt+F10": "Maximize the focused window\n"
      },
      {
        "Control+Alt+D": "Minimize all\n"
      },
      {
        "Control+Alt+D": "Undo minimize all\n"
      },
      {
        "F11": "Switch fullscreen/normal size\n"
      },
      {
        "Control+F11": "Show the window in full screen mode, with no border, menubar, toolbar or statusbar\n"
      },
      {
        "Alt+F12": "Rollup/down window\n"
      },
      {
        "Meta": "Show all open windows\n"
      },
      {
        "Control+`": "Show all windows of current application\n"
      },
      {
        "Meta": "Show all workspaces\n"
      },
      {
        "Control+Alt+Shift+ArrowLeft": "Move window to left/right/up/down workspace\n"
      },
      {
        "ArrowRight": "Move window to left/right/up/down workspace\n"
      },
      {
        "ArrowUp": "Move window to left/right/up/down workspace\n"
      },
      {
        "ArrowDown": "Move window to left/right/up/down workspace\n"
      },
      {
        "Control+Alt+ArrowLeft": "Go to left/right/up/down workspace\n"
      }
    ],
    "KDE": [
      {
        "Alt+F3": "Pop up window menu\n"
      },
      {
        "Alt+F4": "Close the focused window\n"
      },
      {
        "Alt+F3": "Restore the focused window to its previous size\n"
      },
      {
        "Alt": "Move the focused window\n"
      },
      {
        "Alt+F3": "Resize the focused window\n"
      },
      {
        "Alt+F3": "Minimize the focused window\n"
      },
      {
        "Alt+F3": "Maximize the focused window\n"
      },
      {
        "F11": "Switch fullscreen/normal size\n"
      },
      {
        "Control+Shift+F": "Show the window in full screen mode, with no border, menubar, toolbar or statusbar\n"
      },
      {
        "Control+Fn": "Go to workspace n\n"
      },
      {
        "Control+Alt+Escape": "Quit application of current window\n"
      }
    ],
    "macOS": [
      {
        "Meta+F11": "Close the current internet tab\n"
      },
      {
        "⌥ Opt+Alt+Enter+⌥ Option+Alt+F": "Force window mode (Application requires functionality for set action)\n"
      },
      {
        "Meta+F": "Force window mode (Application requires functionality for set action)\n"
      },
      {
        "Meta+⌥ Option+F": "Force window mode (Application requires functionality for set action)\n"
      },
      {
        "Meta+W": "Close the focused window\n"
      },
      {
        "Meta+⌥ Opt+W": "Close all windows of current application\n"
      },
      {
        "Meta+H": "Hide the focused window\n"
      },
      {
        "Meta+⌥ Option+H": "Hide all except the focused window\n"
      },
      {
        "Meta+M": "Minimize the focused window\n"
      },
      {
        "Meta+L": "Maximize the focused window\n"
      },
      {
        "Meta+Alt+M": "Minimize all\n"
      },
      {
        "Meta+Shift+F": "Switch fullscreen/normal size\n"
      },
      {
        "Meta+Control+F": "Show the window in full screen mode, with no border, menubar, toolbar or statusbar\n"
      },
      {
        "F3": "Show all open windows\n"
      },
      {
        "Control+F3": "Show all windows of current application\n"
      },
      {
        "F8": "Show all workspaces\n"
      },
      {
        "Control+N": "Go to workspace n\n"
      },
      {
        "Control+ArrowLeft": "Go to left/right/up/down workspace\n"
      },
      {
        "Meta+Q": "Quit application of current window\n"
      },
      {
        "Escape": "Close dialog\n"
      },
      {
        "Meta+A+Shift": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+F3": "Peek at the desktop\n"
      }
    ],
    "Windows": [
      {
        "Control+W": "Close the current internet tab\n"
      },
      {
        "Alt+Enter": "Force window mode (Application requires functionality for set action)\n"
      },
      {
        "Alt+Space": "Pop up window menu\n"
      },
      {
        "Alt+F4": "Close the focused window\n"
      },
      {
        "Alt+Space": "Close the focused window\n"
      },
      {
        "Alt+Space": "Restore the focused window to its previous size\n"
      },
      {
        "Alt+Space": "Move the focused window\n"
      },
      {
        "Alt+Space": "Resize the focused window\n"
      },
      {
        "Control+Alt+Escape": "Keep window always on top\n"
      },
      {
        "Alt+Escape": "Minimize the focused window\n"
      },
      {
        "Alt+Space": "Minimize the focused window\n"
      },
      {
        "Alt+Space": "Maximize the focused window\n"
      },
      {
        "Meta+Shift+ArrowUp": "Maximize vertically\n"
      },
      {
        "Meta+M": "Minimize all\n"
      },
      {
        "Meta+D": "Minimize all\n"
      },
      {
        "Meta+Home": "Minimize all non focused windows\n"
      },
      {
        "Meta+Shift+M": "Undo minimize all\n"
      },
      {
        "F11": "Switch fullscreen/normal size\n"
      },
      {
        "Meta+Shift+Enter": "Switch fullscreen/normal size\n"
      },
      {
        "Meta+Tab": "Show all open windows\n"
      },
      {
        "Meta+Tab": "Show all workspaces\n"
      },
      {
        "Meta+ArrowLeft": "Move window to left/right/up/down workspace\n"
      },
      {
        "ArrowRight": "Move window to left/right/up/down workspace\n"
      },
      {
        "Meta+Shift+ArrowLeft": "Move window between multiple monitors\n"
      },
      {
        "ArrowRight": "Move window between multiple monitors\n"
      },
      {
        "Alt+F4": "Quit application of current window\n"
      },
      {
        "Control+F4": "Quit application of current window\n"
      },
      {
        "Escape+Meta+ArrowDown+F4": "Close dialog\n"
      },
      {
        "Meta+0": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+1": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+2": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+3": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+4": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+5": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+6": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+7": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+8": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+9": "Open/Focus (preview) pinned program on the taskbar\n"
      },
      {
        "Meta+0": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+1": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+2": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+3": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+4": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+5": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+6": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+7": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+8": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+9": "Open new program window of pinned program in Quick Launch\n"
      },
      {
        "Meta+Shift+0": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+1": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+2": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+3": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+4": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+5": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+6": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+7": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+8": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+Shift+9": "Open new program window of the pinned program on the taskbar (if program is already opened)\n"
      },
      {
        "Meta+T": "Focus the first taskbar entry; pressing again will cycle through them\n"
      },
      {
        "Meta+Space+Fn+Meta": "Peek at the desktop\n"
      },
      {
        "Meta+G+Meta+Space": "Bring gadgets to the front of the Z-order and cycle between gadgets\n"
      },
      {
        "Meta+P": "External display options (mirror, extend desktop, etc.)\n"
      }
    ]
  },
  "User interface navigation (widgets and controls)": {
    "GNOME": [
      {
        "Tab": "Moves keyboard focus to next/previous control\n"
      },
      {
        "Shift+Tab": "Moves keyboard focus to next/previous control\n"
      },
      {
        "Control+F1": "Pop up tooltip for currently focused control\n"
      },
      {
        "Shift+F1": "Show context-sensitive help for currently focused window or control\n"
      },
      {
        "F6": "Give focus to next/previous pane\n"
      },
      {
        "Shift+F6": "Give focus to next/previous pane\n"
      },
      {
        "F8": "Give focus to splitter bar in paned window\n"
      },
      {
        "F10": "Give focus to window's menu bar\n"
      },
      {
        "ContextMenu": "Pop up contextual menu for currently selected objects (aka context menu)\n"
      },
      {
        "Shift+F10": "Pop up contextual menu for currently selected objects (aka context menu)\n"
      },
      {
        "Space": "Toggle selected state of focused checkbox, radio button, or toggle button\n"
      },
      {
        "Enter": "Activate focused button, menu item etc.\n"
      },
      {
        "Home": "Select/move to first/last item in selected widget\n"
      },
      {
        "End": "Select/move to first/last item in selected widget\n"
      },
      {
        "PageUp": "Scroll selected view by one page up/left/down/right\n"
      },
      {
        "Control+PageUp": "Scroll selected view by one page up/left/down/right\n"
      },
      {
        "PageDown": "Scroll selected view by one page up/left/down/right\n"
      },
      {
        "Control+PageDown": "Scroll selected view by one page up/left/down/right\n"
      },
      {
        "Control+Tab": "Switch focus to the next/previous tab within a window\n"
      },
      {
        "Control+Alt+Tab": "Switch focus to the next/previous panel on the desktop\n"
      },
      {
        "Control+Alt+Shift+Tab": "Switch focus to the next/previous panel on the desktop\n"
      },
      {
        "Control+Alt+Escape": "Switch focus to the next/previous panel (without dialog)\n"
      }
    ],
    "KDE": [
      {
        "Tab": "Moves keyboard focus to next/previous control\n"
      },
      {
        "Shift+Tab": "Moves keyboard focus to next/previous control\n"
      },
      {
        "Shift+F1": "Show context-sensitive help for currently focused window or control\n"
      },
      {
        "Alt": "Give focus to window's menu bar\n"
      },
      {
        "ContextMenu": "Pop up contextual menu for currently selected objects (aka context menu)\n"
      },
      {
        "Space": "Toggle selected state of focused checkbox, radio button, or toggle button\n"
      },
      {
        "Enter": "Activate focused button, menu item etc.\n"
      },
      {
        "Home": "Select/move to first/last item in selected widget\n"
      },
      {
        "End": "Select/move to first/last item in selected widget\n"
      }
    ],
    "macOS": [
      {
        "Tab": "Moves keyboard focus to next/previous control\n"
      },
      {
        "Shift+Tab": "Moves keyboard focus to next/previous control\n"
      },
      {
        "Meta+?": "Show context-sensitive help for currently focused window or control\n"
      },
      {
        "Meta+`": "Give focus to next/previous pane\n"
      },
      {
        "Meta+Shift+/": "Give focus to window's menu bar\n"
      },
      {
        "Space": "Toggle selected state of focused checkbox, radio button, or toggle button\n"
      },
      {
        "Space+Enter": "Activate focused button, menu item etc.\n"
      },
      {
        "⇞": "Scroll selected view by one page up/left/down/right\n"
      },
      {
        "⇟": "Scroll selected view by one page up/left/down/right\n"
      },
      {
        "↖+Fn+⌥ Opt+↖": "Scroll selected view to top/bottom\n"
      },
      {
        "Control+Tab": "Switch focus to the next/previous tab within a window\n"
      }
    ],
    "Windows": [
      {
        "Tab": "Moves keyboard focus to next/previous control\n"
      },
      {
        "Shift+Tab": "Moves keyboard focus to next/previous control\n"
      },
      {
        "Shift+F1": "Pop up tooltip for currently focused control\n"
      },
      {
        "Shift+F1": "Show context-sensitive help for currently focused window or control\n"
      },
      {
        "Control+F6": "Give focus to next/previous pane\n"
      },
      {
        "Alt+F6": "Give focus to next/previous pane\n"
      },
      {
        "F10": "Give focus to window's menu bar\n"
      },
      {
        "Alt": "Give focus to window's menu bar\n"
      },
      {
        "Shift+F10": "Pop up contextual menu for currently selected objects (aka context menu)\n"
      },
      {
        "ContextMenu": "Pop up contextual menu for currently selected objects (aka context menu)\n"
      },
      {
        "Space": "Toggle selected state of focused checkbox, radio button, or toggle button\n"
      },
      {
        "Enter": "Activate focused button, menu item etc.\n"
      },
      {
        "F4": "Expand a drop-down list\n"
      },
      {
        "Alt+ArrowDown": "Expand a drop-down list\n"
      },
      {
        "Control+Tab": "Switch focus to the next/previous tab within a window\n"
      }
    ]
  },
  "Accessibility": {
    "GNOME": [
      {
        "Shift": "Allow user to press shortcuts one key at a time\n"
      },
      {
        "Shift": "Stop/slow repeating characters when key is pressed\n"
      }
    ],
    "KDE": [],
    "macOS": [],
    "Windows": [
      {
        "Meta+U": "Utility Manager\n"
      },
      {
        "Left Alt+Left Shift+NumLock": "Use keyboard to control cursor\n"
      },
      {
        "Shift": "Allow user to press shortcuts one key at a time\n"
      },
      {
        "NumLock": "Hear beep when -lock key pressed\n"
      },
      {
        "Right Shift": "Stop/slow repeating characters when key is pressed\n"
      },
      {
        "Control+Meta+C": "Accessibility Shorcut\nThis can be set as Greyscale, Invert Colours, Magnifier\nand more in Ease of Access settings\n\n"
      }
    ]
  }
}
