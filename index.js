const Koa = require('koa')
const app = new Koa()
app.use(require('koa-static')('public'))

const fs=require('fs')

const cheerio=require('cheerio')
function parse() {
  const $ = cheerio.load(fs.readFileSync('downloaded/table.html'))

  function selectorToArray(withinElement,selector) {
    const found= []
    $(withinElement).find(selector).each(function (index,element){
      found.push(this)
    })
    return found
  }
  function contents(withinElement) {
    const found=[]
    $(withinElement).contents().each(function (i,el){
      if(el.type=='text') {
        if(found.length > 0 && found[found.length-1].name === undefined) //if the previous one was also a string, concat to that
        found[found.length-1]+=el.data
        else
        found.push(el.data)
      }
      else if(el.name && el.name!=='kbd') {
        if(found.length > 0 && found[found.length-1].name === undefined) //if the previous one was also a string, concat to that
          found[found.length-1]+=$(this).text()
        else found.push($(this).text())
      }
      else found.push(this)
    })
    return found
  }
  const content=$('#content')

  const tables=selectorToArray(content,'table.wikitable')
  function processTable(table,tableName) {
    console.log(tableName)
    const rows=selectorToArray(table,'tr')
    const systems=selectorToArray(rows[0],'th').map(t=>$(t).text()
    .replace('\n','')//remove newlines
    .replace(/\s?\[[^\]]+\]/,'')//remove links
    .replace(/[^\s]\(/,' (') //add a space before parens if there isn't already so word wrapping works when displaying
    .replace(/\s*\/\s*/,'/') // remove spaces between two words and the / separator
    )
    systems.shift() //remove "Actions" header
    rows.shift() //remove systems row
    const cellRows=rows.map(r=>selectorToArray(r,'th, td'))

    function categorizeText(txt) {
      const sanitized=txt.type? $(txt).text().toLowerCase() : txt.toLowerCase()
      if(/^,?\s+or\s+$/.test(sanitized) || /^\s*\/\s*$/.test(sanitized)) return '///'
      if(sanitized.includes('then')) return '...'
      return ''
    }
    function reduceCellArr(accum,curr,isBreak) {
      if(curr==='...' || isBreak) return accum.arr
      //if it's a ///, that indicates an alternative shortcut
      if(curr==='///') {console.log('ok'); return [...accum.arr,'']; }
      //if it's blank don't do anything
      if(curr==='') return accum.arr
      //otherwise concatenate the string to the last one in the array (add the key to the current key combo)
      if(curr==='+') curr=`'+'` //note: + key is surrounded with quotes to differentiate it from the + key separator
      //todo: make sure in the UI you are taking the + key quotes into account during splitting: .split(/[^']\+[^']/)
      //todo: make sure in teh UI you are taking the " " and calling it "Space" in the data object
      
      const lastThing=accum.arr.length > 0 ? accum.arr.pop() +'+' : ''
      const lastThingReally=lastThing=='+'?'':lastThing //lastThing could just be a '+' by itself
      //make key combos for all the possible values of the below wildcard symbols
      if(curr=='(#)') return [...accum.arr,...('0123456789'.split('').map(n=>lastThingReally+n))]
      if(curr=='<letter>') return [...accum.arr,...('ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('').map(n=>lastThingReally+n))]

      return [...accum.arr,lastThingReally+curr]
    }
    function splitCell(cell) {
      const encoded= contents(cell)
        .map(el=>el.name =='kbd' ? deDupeKey($(el).text()) : categorizeText(el))
        .reduce((accum,curr)=>({
          break:accum.break || curr==='...',
          arr:reduceCellArr(accum,curr,accum.break)
        }),{
          break:false,
          arr:[] //an array of arrays of keys
        })
        console.log(encoded.arr)
      return encoded.arr
    }
    function getShortcutsForSystem(sysIndex) {
      return cellRows.reduce((accum,curr)=>[
        ...accum,
        ...splitCell(curr[sysIndex+1]).map(c=>({[c.replace(/\s+/,' ')]:$(curr[0]).text().replace(/\s+/,' ')}))
      ],[])
    }
    // fs.writeFileSync('downloaded/cellRows.json',JSON.stringify(cellRows,null,2))
    const shortcuts=systems.reduce((accum,curr,i)=>({[curr]:getShortcutsForSystem(i),...accum}),{})
    fs.writeFileSync('downloaded/shortcuts.json',JSON.stringify(shortcuts,null,2))
    return shortcuts
  }
  const categories=selectorToArray(content,'h3,h4')
    .map(t=>$(t).text())
    .map(t=>t.replace('[edit]',''))

  const keys=Array.from(new Set([
    ...selectorToArray(content,'kbd')
    .map(t=>$(t).text())
    .map(deDupeKey),
    ...('ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.split('')),
    ...('F1 F2 F3 F4 F5 F6 F7 F8 F9 F10 F11 F12'.split(' '))
  ]))
  keys.sort()
  return {
    shortcuts:categories.reduce((accum,curr,i)=>({...accum,[curr]:processTable(tables[i],curr)}),{}),
    keys
  }
}
function deDupeKey(key) {
  const sanitized=key.toLowerCase()
  if(sanitized.includes('enter') || sanitized.includes('return')) return "Enter"
  if(sanitized.includes('backspace')) return "Backspace"
  if(sanitized.includes('esc')) return "Escape"
  if('abcdefghijklmnopqrstuvwxyz'.split('').includes(key)) return key.toUpperCase()
  if(sanitized.includes('meta') || sanitized.includes('cmd')) return 'Meta'
  if(sanitized =='⇧ shift') return 'Shift'
  if(sanitized.includes('del')) return 'Delete'
  if(sanitized == 'ctrl/core' || sanitized == 'ctrl') return 'Control'
  if(sanitized == 'prtscn' || sanitized == 'print screen') return 'PrintScreen'
  if(sanitized == 'numlock') return 'NumLock'
  if(sanitized == 'function') return 'Fn'
  if(sanitized == 'page down' || sanitized == 'pagedown') return 'PageDown'
  if(sanitized == 'page up' || sanitized == 'pageup') return 'PageUp'
  if(sanitized == "←") return "ArrowLeft"
  if(sanitized == "↑") return "ArrowUp"
  if(sanitized == "→") return "ArrowRight"
  if(sanitized == "↓") return "ArrowDown"
  if(sanitized.includes('tab')) return 'Tab'
  if(sanitized.includes('⊞') || sanitized.includes('Win')) return 'Meta'
  if(sanitized.includes("≣")) return "ContextMenu"
  return key
}

const availableKeys=["Enter","Tab"," ","ArrowDown","ArrowLeft","ArrowRight","ArrowUp",
"End","Home","PageDown","PageUp","Backspace","Clear","Copy","CrSel","Cut","Delete","EraseOf","ExSel","Insert","Paste","Redo","Undo",
"Escape","Find","Help","Pause","Play","ContextMenu",
"F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","F13","F14","F15","F16","F17","F18","F19","F20",
"PrintScreen","MediaFastForward","MediaPause","MediaPlay","MediaPlayPause","MediaRecord","MediaRewind","MediaStop",
"MediaTrackNext","MediaTrackPrevious","Multiply","Add","Clear","Divide","Subtract",
"Alt","CapsLock","Control","Fn","Hyper","FnLock","Meta","NumLock","ScrollLock","Shift","Super","Symbol","SymbolLock",
..."`~1!2@3#4$5%6^7&8*9(0)-_=+[{]};:'\"\\|<>,./?ABCDEFGHIJKLMNOPQRSTUVWXYZ".split('')]
const result=parse()
delete result.shortcuts['Command line shortcuts']
fs.writeFileSync('public/data.js',`
const keys=${JSON.stringify(availableKeys)}
const data=${JSON.stringify(result.shortcuts,null,2)}
`)
fs.writeFileSync('downloaded/keys.json',JSON.stringify(result.keys,null,2))
app.listen(3000,()=>{
  console.log('listening on 3000')
})
//TODO: within reduceCellArr, if it's a slash then an arrow, look to see if the previous one was an arrow with modifier(s) and if so, give the current arrow the same modifier(s)

